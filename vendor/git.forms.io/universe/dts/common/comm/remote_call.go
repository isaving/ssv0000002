//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package comm

import (
	"fmt"
	"git.forms.io/universe/comm-agent/client"
	common "git.forms.io/universe/comm-agent/common/protocol"
	util2 "git.forms.io/universe/common/util"
	"git.forms.io/universe/dts/common/const"
	"git.forms.io/universe/dts/common/log"
	"git.forms.io/universe/solapp-sdk/compensable"
	"git.forms.io/universe/solapp-sdk/util"
	jsoniter "github.com/json-iterator/go"
	"github.com/pkg/errors"
	"github.com/valyala/fasthttp"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"time"
)

const (
	HttpContentTypeKey = "Content-Type"
	ContentTypeJson    = "application/json"
	HttpMethodPost     = "POST"
)

// @Desc a interface contains HttpPost method and DTSRequestWithEvent to send message
type Client interface {
	HttpPost(dtsCtx *compensable.TxnCtx, address string, request interface{}, response interface{}, replyHeader *map[string]string, timeoutMilliseconds int, maxRetryTimes int) error
	DTSRequestWithEvent(spanCtc *compensable.SpanContext, dtsCtx *compensable.TxnCtx, topicAttributes map[string]string, event interface{}, reply interface{}, timeoutMilliseconds int, maxRetryTimes int) error
}

// @Desc new a RemoteCallClientFactory
func NewRemoteClientFactory() *RemoteCallClientFactory {
	return &RemoteCallClientFactory{}
}

// @Desc common Response structure
type CommonResponse struct {
	ErrorCode int //-1 indicat error ,0 indicat success
	ErrorMsg  string
	Data      interface{}
}

// @Desc a interface contains a CreateClient method
type ClientFactory interface {
	CreateClient() Client
}

type RemoteCallClient struct{}

type RemoteCallClientFactory struct{}

// @Desc Build DTS Topic Attributes
// @Param dstOrg the org of the destination
// @Param dstDcn the org of the destination
// @Param nodeId
// @Param instanceId
// @Param dstTopicId the topicID of the destination
// @Return DTSTopicAttributes
// @Return error
//func BuildDTSTopicAttributes(srcNodeId, srcInsId, dstOrg, dstDcn, nodeId, instanceId, dstTopicId string) (map[string]string, error) {
func BuildDTSTopicAttributes(dstOrg, dstDcn, nodeId, instanceId, dstTopicId string) (map[string]string, error) {
	attributeMap := make(map[string]string)
	if dstOrg == "" || dstDcn == "" || dstTopicId == "" {
		return attributeMap, fmt.Errorf("dstOrg=%s,dstDcn=%s,dstTopicId=%s have empty field", dstOrg, dstDcn, dstTopicId)
	}

	attributeMap[common.TOPIC_TYPE] = common.TOPIC_TYPE_DTS
	attributeMap[common.TOPIC_DESTINATION_ORG] = dstOrg
	attributeMap[common.TOPIC_DESTINATION_DCN] = dstDcn
	attributeMap[common.TOPIC_DESTINATION_NODE_ID] = nodeId
	attributeMap[common.TOPIC_DESTINATION_INSTANCE_ID] = instanceId
	attributeMap[common.TOPIC_ID] = dstTopicId

	return attributeMap, nil
}

// @Desc new a Client
func (T *RemoteCallClientFactory) CreateClient() Client {
	return &RemoteCallClient{}
}

// @Desc pub event message and mesh address
// @Param address mesh address
// @Param request event message
// @Return err error
func (T *RemoteCallClient) PubEvent(address string, request string) (err error) {
	log.Debugf("Pub event message[%s] to Mesh(address:[%s])", request, address)
	return
}

// @Desc send request with event
// @Param commAgentAddress comm-agent address
// @Param topicName topicID
// @Param bodyType Content-Type
// @Param requestBody request data
// @Return response responseBody
// @Return err error
func (T *RemoteCallClient) RequestWithEvent(commAgentAddress string, topicName string, bodyType string, requestBody string) (response string, err error) {
	client := &http.Client{}

	request, err := http.NewRequest(HttpMethodPost, commAgentAddress, strings.NewReader(requestBody))
	if err != nil {
		return
	}
	request.Header.Add("sync", "true")
	request.Header.Add("topic", fmt.Sprintf("%s/001",
		topicName,
	))

	if "" != bodyType {
		request.Header.Set(HttpContentTypeKey, bodyType)
	}
	resp, err := client.Do(request)
	if err != nil {
		//log.Errorf("Do http post failed: %s", err.Error())
		return
	}

	if resp.StatusCode != 200 {
		err = errors.Errorf("%d:%s", resp.StatusCode, resp.Status)
		//log.Errorf("Http post failed,url:[%s], error :[%s]", url, err.Error())
		return
	}

	defer func() { _ = resp.Body.Close() }()
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//log.Errorf("Do http post failed: %s", err.Error())
		return
	}

	response = string(responseBody)
	return
}

// @Desc send TRN request with event
// @Param dtsCtx dts context
// @Param commAgentAddress comm-agent address
// @Param topicName topicID
// @Param bodyType Content-Type
// @Param requestBody request data
// @Return response responseBody
// @Return err error
func (T *RemoteCallClient) TrnReqWithEvent(dtsCtx *compensable.TxnCtx, commAgentAddress string, topicSource string, topicTarget string, bodyType string, requestBody string) (response string, err error) {
	client := &http.Client{}

	request, err := http.NewRequest(HttpMethodPost, commAgentAddress, strings.NewReader(requestBody))
	if err != nil {
		return
	}
	request.Header.Add("sync", "true")
	request.Header.Add("topic", fmt.Sprintf("TRN/%s/%s/001",
		topicSource,
		topicTarget,
	))

	if nil != dtsCtx {
		request.Header.Add(constant.TXN_PROPAGATE_ROOT_XID_KEY, dtsCtx.RootXid)
		request.Header.Add(constant.TXN_PROPAGATE_PARENT_XID_KEY, dtsCtx.ParentXid)
		request.Header.Add(constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS, dtsCtx.DtsAgentAddress)
	}
	// 事务传播

	if "" != bodyType {
		request.Header.Set(HttpContentTypeKey, bodyType)
	}
	resp, err := client.Do(request)
	if err != nil {
		//log.Errorf("Do http post failed: %s", err.Error())
		return
	}

	if resp.StatusCode != 200 {
		err = errors.Errorf("%d:%s", resp.StatusCode, resp.Status)
		//log.Errorf("Http post failed,url:[%s], error :[%s]", url, err.Error())
		return
	}

	defer func() { _ = resp.Body.Close() }()
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//log.Errorf("Do http post failed: %s", err.Error())
		return
	}

	response = string(responseBody)
	return
}

// @Desc send request with HTTP
// @Param dtsCtx dts context
// @Param url request's url
// @Param request request body data
// @Param requestHeader request Header data
// @Param response request data
// @Param replyHeader reply Header data
// @Param timeout time-out time
// @Return err error
func (T *RemoteCallClient) HttpPost(dtsCtx *compensable.TxnCtx, url string, request interface{}, response interface{}, replyHeader *map[string]string, timeoutMilliseconds int, maxRetryTimes int) (err error) {
	json := jsoniter.ConfigCompatibleWithStandardLibrary

	client := &http.Client{
		Timeout: time.Millisecond * time.Duration(timeoutMilliseconds),
	}
	requestBytes, err := json.Marshal(request)
	if err != nil {
		log.Errorf("commRequestWithEvent Marshal request failed err=%v", err)
		return
	}
	log.Debugf("Request with [%s]", string(requestBytes))
	req, err := http.NewRequest("POST", url, strings.NewReader(string(requestBytes)))
	if err != nil {
		return
	}

	// to avoid app properties override dts properties!
	if dtsCtx != nil {
		req.Header.Add(constant.TXN_PROPAGATE_ROOT_XID_KEY, dtsCtx.RootXid)
		req.Header.Add(constant.TXN_PROPAGATE_PARENT_XID_KEY, dtsCtx.ParentXid)
		req.Header.Add(constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS, dtsCtx.DtsAgentAddress)
	}

	req.Header.Set(HttpContentTypeKey, ContentTypeJson)

	tryCounts := 0
	var resp *http.Response
	for {
		tryCounts++
		resp, err = client.Do(req)
		if util2.IsError(err, fasthttp.ErrTimeout) {
			if tryCounts <= maxRetryTimes {
				log.Errorf("HttpPost=>Request DTS Agent timeout[error=%v], try counts[%d], try again...", util2.ErrorToString(err), tryCounts)
				continue
			} else {
				log.Error("HttpPost=>Request DTS Agent failed. ")
				return err
			}
		} else if nil != err {
			return err
		} else {
			break
		}
	}

	if resp.StatusCode != 200 {
		err = errors.Errorf("%d:%s", resp.StatusCode, resp.Status)
		return
	}
	if nil != response {
		defer func() { _ = resp.Body.Close() }()
		responseBody, e := ioutil.ReadAll(resp.Body)
		if e != nil {
			err = e
			return
		}
		replyHeader := make(map[string]string)
		for k, vs := range resp.Header {
			if len(vs) > 0 {
				replyHeader[k] = vs[0]
			}
		}

		err = json.Unmarshal(responseBody, response)
	}
	return
}

// @Desc send DTS request with event
// @Param spanCtx span context
// @Param dtsCtx dts context
// @Param dstOrg the org of destination
// @Param dstDcn the dcn of destination
// @Param nodeId
// @Param instanceId
// @Param topicId
// @Param event event data
// @Param reply reply data
// @Param timeout timeout time
// @Return err error
func (T *RemoteCallClient) DTSRequestWithEvent(spanCtx *compensable.SpanContext, dtsCtx *compensable.TxnCtx, topicAttributes map[string]string, event interface{}, reply interface{}, timeoutMilliseconds int, maxRetryTimes int) (err error) {
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	if nil != reply {
		if kind := reflect.ValueOf(reply).Type().Kind(); kind != reflect.Ptr {
			err = errors.New("reply param should be pointer type")
			return
		}
	}

	requestBytes, err := json.Marshal(event)
	if err != nil {
		log.Errorf("commRequestWithEvent Marshal request failed err=%v", err)
		return
	}

	//attributes, _ := BuildDTSTopicAttributes(dstOrg, dstDcn, nodeId, instanceId, topicId)

	msg := &client.UserMessage{
		AppProps:       make(map[string]string),
		TopicAttribute: topicAttributes,
		Body:           requestBytes,
	}

	var parentSpanId string
	if nil != spanCtx {
		msg.AppProps[constant.TRACE_ID_KEY] = spanCtx.TraceId
		if "" != spanCtx.SpanId {
			parentSpanId = spanCtx.SpanId
		} else {
			parentSpanId = spanCtx.TraceId
		}
	}

	msg.AppProps[constant.PARENT_SPAN_ID_KEY] = parentSpanId
	msg.AppProps[constant.SPAN_ID_KEY] = util.GenerateSerialNo("1")
	tryCounts := 0
	var ret *client.UserMessage
	for {
		tryCounts++
		ret, err = client.SendRequestMessageV2(msg, timeoutMilliseconds)
		if util2.IsError(err, fasthttp.ErrTimeout) {
			if tryCounts <= maxRetryTimes {
				log.Errorf("DTSRequestWithEvent=>Request DTS Agent timeout[error=%v], try counts[%d], try again...", util2.ErrorToString(err), tryCounts)
				continue
			} else {
				log.Error("DTSRequestWithEvent=>Request DTS Agent failed. ")
				return err
			}
		} else if nil != err {
			return err
		} else {
			break
		}
	}

	//log.Debugf("Response:[%++v], Body=[%s]", ret, string(ret.Body))
	err = json.Unmarshal(ret.Body, reply)

	return
}
