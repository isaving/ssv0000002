//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package util

import (
	"fmt"
	"git.forms.io/universe/common/util"
	"sync"
	"time"
)

type Future struct {
	isFinished bool
	result     interface{}
	resultChan chan interface{}
	l          sync.Mutex
}

func (f *Future) GetResult(timeout int) interface{} {
	f.l.Lock()
	defer f.l.Unlock()
	if f.isFinished {
		return f.result
	}
	timer := util.AcquireTimer(time.Second * time.Duration(timeout))
	defer util.ReleaseTimer(timer)

	select {
	case <-timer.C:
		f.isFinished = true
		f.result = nil
		return fmt.Errorf("get result timeout: %d s", timeout)
	case f.result = <-f.resultChan:
		f.isFinished = true
		return f.result
	}

}

func (f *Future) SetResult(result interface{}) {
	if f.isFinished {
		return
	}
	f.resultChan <- result
	close(f.resultChan)
}

//func NewFuture(timer *time.Timer, duration time.Duration) *Future {
func NewFuture() *Future {
	return &Future{
		isFinished: false,
		result:     nil,
		resultChan: make(chan interface{}, 1),
	}
}
