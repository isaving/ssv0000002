//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package client

import (
	"bytes"
	"encoding/json"
	log "git.forms.io/universe/comm-agent/common/log"
	proto "git.forms.io/universe/comm-agent/common/protocol"
	"github.com/go-errors/errors"
	"io/ioutil"
	"net/http"
	"reflect"
	"time"
)

const (
	// url path for request/reply message
	SEND_REQUEST_REPLY_MSG_PATH = "/v1/send-request-reply-msg"

	// url path for reply message
	SEND_REPLY_MSG_PATH = "/v1/send-reply-msg"

	// url path for publish message
	SEND_TOPIC_MSG_PATH = "/v1/send-topic-msg"

	// url path for ack a queue message
	SEND_QUEUE_ACK_PATH = "/v1/send-queue-ack"

	// url path for semi-sync call message
	REPLY_SEMI_SYNC_CALL_PATH = "/v1/reply-semi-sync-call"
)

// define global variable for storing comm-agent server address(http address)
// default address is "http://127.0.0.1:18080"
var commServerAddr = "http://127.0.0.1:18080"

// define global variable for storing comm-agent server address(unix socket address)
// default address is "http://unix"
//var unixSocketHttpAddr = "http://unix"

// use for tcp/ip socket
var httpClient *http.Client

// use for UNIX socket
//var httpClient2 *http.Client

// InitCommClient init httpClient & httpClient2, create handle for http transport
func InitCommClient() {
	transport := http.Transport{
		// set disable keep alive to false, request can reuse the connection
		// default is true, if true, disables HTTP keep-alives and
		// will only use the connection to the server for a single
		// HTTP request.
		DisableKeepAlives: false,
	}
	httpClient = &http.Client{
		Transport: &transport,
	}
	//httpClient2 = &http.Client{
	//	Transport: &http.Transport{
	//		// set disable keep alive to false, request can reuse the connection
	//		// default is true, if true, disables HTTP keep-alives and
	//		// will only use the connection to the server for a single
	//		// HTTP request.
	//		DisableKeepAlives: false,
	//
	//		DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
	//			return net.Dial("unix", config.CommAgentCfg.C2sSockFile)
	//		},
	//	},
	//}
}

// SetCommServerAddr is for application to specify comm-agent server address
// if addr is empty, default value "http://127.0.0.1:18080" will be set.
func SetCommServerAddr(addr string) {
	if addr != "" {
		commServerAddr = addr
	}
}

// isValidPointer determine if a response pointer is valid
func isValidPointer(response interface{}) error {
	if nil == response {
		return errors.New("input param should not be nil")
	}

	if kind := reflect.ValueOf(response).Type().Kind(); kind != reflect.Ptr {
		return errors.New("input param should be pointer type")
	}

	return nil
}

// postRequest is a internal function for client post request to server
// and it's returns responses where from server endpoint
// fixed http request timeout is 30 seconds
func postUserRequest(path string, req interface{}, resp interface{}) error {
	commonResp := proto.CommonResponse{}
	requestBytes, err := json.Marshal(req)
	if err != nil {
		return errors.Wrap(err, 0)
	}
	var fullUrl string
	//if config.CommAgentCfg.IsConnectWithUnixSocket() {
	//	fullUrl = unixSocketHttpAddr + path
	//} else {
	fullUrl = commServerAddr + path
	//}

	httpRequest, err := http.NewRequest(http.MethodPost, fullUrl, bytes.NewReader(requestBytes))
	if err != nil {
		log.Errorf("postUserRequest failed, url:%s, err=%v", fullUrl, err)
		return errors.Wrap(err, 0)
	}
	var httpResponse *http.Response
	//if config.CommAgentCfg.IsConnectWithUnixSocket() {
	//	httpClient2.Timeout = time.Second * 30
	//	httpResponse, err = httpClient2.Do(httpRequest)
	//} else {
	httpClient.Timeout = time.Second * 30
	httpResponse, err = httpClient.Do(httpRequest)
	//}
	if err != nil {
		return errors.Wrap(err, 0)
	}

	defer httpResponse.Body.Close()

	if httpResponse.StatusCode != 200 {
		//return fmt.Errorf("postUserRequest StatusCode != 200,fullUrl=%s,statusCode=%v", fullUrl, httpResponse.StatusCode)
		return errors.Errorf("postUserRequest StatusCode != 200,fullUrl=%s,statusCode=%v", fullUrl, httpResponse.StatusCode)
	}

	body, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		return errors.Wrap(err, 0)
	}

	err = json.Unmarshal(body, &commonResp)
	if err != nil {
		return errors.Wrap(err, 0)
	}

	if commonResp.ErrorCode != 0 {
		//return fmt.Errorf("%v", commonResp.ErrorMsg)
		return errors.Errorf("%v", commonResp.ErrorMsg)
	}

	//do not need response
	if err := isValidPointer(resp); err != nil {
		return nil
	}

	datas, err := json.Marshal(commonResp.Data)
	if err != nil {
		//return fmt.Errorf("postUserRequest marsh data err=%v", err)
		return errors.Errorf("postUserRequest marsh data err=%v", err)
	}

	err = json.Unmarshal([]byte(datas), resp)

	return errors.Wrap(err, 0)
}
