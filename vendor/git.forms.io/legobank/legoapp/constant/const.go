package constant

//Global system variables are defined in the header
const (
	PARENT_SPAN_ID = "ParentBizSeqNo"

	GLOBALBIZSEQNO    = "GlobalBizSeqNo"    //Global unique business serial number
	ORGSYSID          = "OrgSysId"          //Original system ID (unchanged throughout transaction flow)
	ORGCHANNELTYPE    = "OrgChannelType"    //Business origination channel type (unchanged throughout the transaction flow)
	ORGSCENARIOID     = "OrgScenarioId"     //Business initiation scenario number (unchanged throughout the entire transaction flow)
	ORGPARTNERID      = "OrgPartnerId"      //Identification of the partner who initiated the business request
	SRCSYSID          = "SrcSysId"          //Source system code
	SRCDCN            = "SrcDcn"            //Source system DCN
	SRCSYSVERSION     = "SrcSysVersion"     //Source system version
	SRCSERVERID       = "SrcServerId"       //Service consumer server identification (server name or ip address)
	SRCSERVICEID      = "SrcServiceId"      //Service consumer service identification
	SRCTIMESTAMP      = "SrcTimeStamp"      //Source system initiates service call timestamp
	SRCTOPOICID       = "SrcTopoicId"       //Source service ID (Topoic or URL)
	SRCBIZSEQNO       = "SrcBizSeqNo"       //Source service business flow
	SRCBIZDATE        = "SrcBizDate"        //Source Service Business Date
	TXWORKSTATION     = "TxWorkStation"     //Trading terminal (logical terminal)
	TXDEVICEID        = "TxDeviceId"        //Transaction Initiating Device Terminal Number (eg. IMEI Code)
	TXDEPTCODE        = "TxDeptCode"        //Trading department (branch line number)
	REVERSESEQNO      = "ReverseSeqNo"      //This field is required if it is a rectification service
	USERLANG          = "UserLang"          //ISO International Language Code
	TXUSERID          = "TxUserId"          //Teller user id
	TXUSERLVL         = "TxUserLvl"         //Teller user level
	TXAUTHORIZER      = "TxAuthorizer"      //Authorizer
	TXAUTHUSERID      = "TxAuthUserId"      //Authorized user id
	TXAUTHUSERLVL     = "TxAuthUserLvl"     //Authorized user level
	TXAUTHFLAG        = "TxAuthFlag"        //Authorization flag (there is a service definition that requires authorization) The business system requires that a specific authorization flag bit be set when authorization is performed, and the front-end authorization returns the authorization flag.
	TRGSERVERID       = "TrgServerId"       //Target system server ID (server name or IP address)
	TRGSERVICEID      = "TrgServiceId"      //Target service ID (Topoic or URL)
	TRGSERVICEVERSION = "TrgServiceVersion" //Target service version
	TRGTOPOICID       = "TrgTopoicId"       //Target Service ID (Topoic)
	TRGBIZDATE        = "TrgBizDate"        //Target system service business date
	TRGBIZSEQNO       = "TrgBizSeqNo"       //Target system business flow
	TRGTIMESTAMP      = "TrgTimeStamp"      //This field must be filled in when returning
	LASTACTENTRYNO    = "LastActEntryNo"    //Maintained by the account center, each system is transparent, and must be digital
	RETSTATUS         = "RetStatus"         //Value range: N-successful transaction F-failed transaction This field must be filled in when returning
	IS_NEED_LOOKUP    = "_is_need_lookup"
)

//Define common error message structures
const (
	RETMSGCODE  = "RetMsgCode"
	RETAUTHLVL  = "RetAuthLvl"
	RETMESSAGE  = "RetMessage"
	ERRORFORMID = "RETMSG"
)

//Define default language
const (
	ENUS = "en-US"
)

//Define common error codes
const (
	SYSPANIC = "SY00000001"
	SYSCODE1 = "SY00000002"

	SYTIMEOUT   = "SY00000003"
	SYERRCONT   = "SY00000004"
	KEYCONFLICT = "SY00000005"

	REQPACKERR   = "SY00000006"
	REQUNPACKERR = "SY00000007"
	RSPPACKERR   = "SY00000008"
	RSPUNPACKERR = "SY00000009"

	PROXYREGFAILD = "SY00000010" //Register DTS Proxy failed. %v
	PROXYFAILD    = "SY00000011" //DTS proxy executed service failed, %v
	REMOTEFAILD   = "SY00000012" //Remote call %s failed, error: %v
	INVALIDERR    = "SY00000013" //
	NOTFOUNDTOPIC = "SY00000014" //Can't found destination topic. serviceKey: [%v]

	DASNOTFOUND = "DA00000002"
)

const (
	TopicPrefix          = "topics::"
	RequestFormIdPrefix  = "requestForm::"
	ResponseFormIdPrefix = "responseForm"
)

//Define common header
const (
	DTS_TOPIC_TYPE = "DTS"
	TRN_TOPIC_TYPE = "TRN"
	KEY_TYPE_RSA   = "RSA"
	KEY_TYPE_AES   = "AES"
	DLS_TYPE_CMM   = "CMM"
	DLS_ID_COMMON  = "common"
	DLS_TYPE_CON   = "CON"
	DLS_TYPE_ACC   = "ACC"
	DLS_TYPE_CUS   = "CUS"
	DLS_TYPE_ACG   = "ACG"
	DLS_TYPE_PRD   = "PRD"
	DLS_TYPE_PHN   = "PHN"
	DLS_TYPE_IDN   = "IDN"
	DLS_TYPE_ODR   = "ODR" //FOR ORDER
	DLS_TYPE_MER   = "MER" //FOR merchant
	DLS_TYPE_PPS   = "PPS" //For PP service
	DLS_TYPE_HUI   = "HUI" //For HU id
	DLS_TYPE_ORG   = "ORG" //For organization
	DLS_TYPE_HCD   = "HCD" //For Hcode
	DLS_TYPE_WID   = "WID" //For wallet WalletId
	DLS_TYPE_CNT   = "CNT" // 合同号切片类型
	DLS_TYPE_DBT    = "DBT" // 借据号切片类型
)

//Request is the struct which describing the format from client
//the whole struct should be stored in SMF body
type Request struct {
	//Param is the param in HTTP request
	//especially useful in GET request
	Param map[string]string `json:"param"`
	//Header is the header will be put on HTTP request
	Header map[string]string `json:"header"`
	//Body is the body will be put on HTTP request
	Body []byte `json:"body"`
}

//Response is the struct which will be returned to client
//the whole struct will be stored in SMF body
type Response struct {
	//Status represent the HTTP status code, eg 200,401
	Status int `json:"status"`
	//Header represent HTTP header return from server
	Header map[string]interface{} `json:"header"`
	//Body represent HTTP body return from server
	Body []byte `json:"body"`
}

const HTTPSTATUSCODE = "httpStatusCode"
const (
	TIME_STAMP        = "2006-01-02 15:04:05"
	DATE_DASH_FORMAT  = "2006-01-02"
	DATEFORMAT        = "20060102"
	TIME_COLON_FORMAT = "15:04:05"
)

const (
	ContentType = "Content-Type"
	JsonContent = "application/json; charset=UTF-8"
)

const PaotangSuccess = "0000"

const BuddhistYearDiffer = 543

// ISO4217List is the list of ISO currency codes
var ISO4217List = map[string]struct{}{
	"AED": {}, "AFN": {}, "ALL": {}, "AMD": {}, "ANG": {}, "AOA": {}, "ARS": {}, "AUD": {}, "AWG": {}, "AZN": {},
	"BAM": {}, "BBD": {}, "BDT": {}, "BGN": {}, "BHD": {}, "BIF": {}, "BMD": {}, "BND": {}, "BOB": {}, "BOV": {}, "BRL": {}, "BSD": {}, "BTN": {}, "BWP": {}, "BYN": {}, "BZD": {},
	"CAD": {}, "CDF": {}, "CHE": {}, "CHF": {}, "CHW": {}, "CLF": {}, "CLP": {}, "CNY": {}, "COP": {}, "COU": {}, "CRC": {}, "CUC": {}, "CUP": {}, "CVE": {}, "CZK": {},
	"DJF": {}, "DKK": {}, "DOP": {}, "DZD": {},
	"EGP": {}, "ERN": {}, "ETB": {}, "EUR": {},
	"FJD": {}, "FKP": {},
	"GBP": {}, "GEL": {}, "GHS": {}, "GIP": {}, "GMD": {}, "GNF": {}, "GTQ": {}, "GYD": {},
	"HKD": {}, "HNL": {}, "HRK": {}, "HTG": {}, "HUF": {},
	"IDR": {}, "ILS": {}, "INR": {}, "IQD": {}, "IRR": {}, "ISK": {},
	"JMD": {}, "JOD": {}, "JPY": {},
	"KES": {}, "KGS": {}, "KHR": {}, "KMF": {}, "KPW": {}, "KRW": {}, "KWD": {}, "KYD": {}, "KZT": {},
	"LAK": {}, "LBP": {}, "LKR": {}, "LRD": {}, "LSL": {}, "LYD": {},
	"MAD": {}, "MDL": {}, "MGA": {}, "MKD": {}, "MMK": {}, "MNT": {}, "MOP": {}, "MRO": {}, "MUR": {}, "MVR": {}, "MWK": {}, "MXN": {}, "MXV": {}, "MYR": {}, "MZN": {},
	"NAD": {}, "NGN": {}, "NIO": {}, "NOK": {}, "NPR": {}, "NZD": {},
	"OMR": {},
	"PAB": {}, "PEN": {}, "PGK": {}, "PHP": {}, "PKR": {}, "PLN": {}, "PYG": {},
	"QAR": {},
	"RON": {}, "RSD": {}, "RUB": {}, "RWF": {},
	"SAR": {}, "SBD": {}, "SCR": {}, "SDG": {}, "SEK": {}, "SGD": {}, "SHP": {}, "SLL": {}, "SOS": {}, "SRD": {}, "SSP": {}, "STD": {}, "SVC": {}, "SYP": {}, "SZL": {},
	"THB": {}, "TJS": {}, "TMT": {}, "TND": {}, "TOP": {}, "TRY": {}, "TTD": {}, "TWD": {}, "TZS": {},
	"UAH": {}, "UGX": {}, "USD": {}, "USN": {}, "UYI": {}, "UYU": {}, "UZS": {},
	"VEF": {}, "VND": {}, "VUV": {},
	"WST": {},
	"XAF": {}, "XAG": {}, "XAU": {}, "XBA": {}, "XBB": {}, "XBC": {}, "XBD": {}, "XCD": {}, "XDR": {}, "XOF": {}, "XPD": {}, "XPF": {}, "XPT": {}, "XSU": {}, "XTS": {}, "XUA": {}, "XXX": {},
	"YER": {},
	"ZAR": {}, "ZMW": {}, "ZWL": {},
}

// 固定错误码定义
const (
	ERRCODE1  = "IL99000001"
	ERRCODE2  = "IL99000002"
	ERRCODE3  = "IL99000003"
	ERRCODE4  = "IL99000004"
	ERRCODE5  = "IL99000005"
	ERRCODE6  = "IL99000006"
	ERRCODE7  = "IL99000007"
	ERRCODE8  = "IL99000008"
	ERRCODE9  = "IL99000009"
	ERRCODE10 = "IL99000010"
	ERRCODE11 = "IL99000011"
	ERRCODE12 = "IL99000012"

	ERRCODE_IL1   = "IL98000001"
	ERRCODE_IL2   = "IL98000002"
	ERRCODE_IL3   = "IL98000003"
	ERRCODE_IL4   = "IL98000004"
	ERRCODE_IL5   = "IL98000005"
	ERRCODE_IL6   = "IL98000006"
	ERRCODE_IL7   = "IL98000007"
	ERRCODE_IL8   = "IL98000008"
	ERRCODE_IL9   = "IL98000009"
	ERRCODE_IL10  = "IL98000010"
	ERRCODE_IL11  = "IL98000011"
	ERRCODE_IL12  = "IL98000012"
	ERRCODE_IL13  = "IL98000013"
	ERRCODE_IL14  = "IL98000014"
	ERRCODE_IL15  = "IL98000015"
	ERRCODE_IL16  = "IL98000016"
	ERRCODE_IL17  = "IL98000017"
	ERRCODE_IL18  = "IL98000018"
	ERRCODE_IL19  = "IL98000019"
	ERRCODE_IL20  = "IL98000020"
	ERRCODE_IL21  = "IL98000021"
	ERRCODE_IL22  = "IL98000022"
	ERRCODE_IL23  = "IL98000023"
	ERRCODE_IL24  = "IL98000024"
	ERRCODE_IL25  = "IL98000025"
	ERRCODE_IL26  = "IL98000026"
	ERRCODE_IL27  = "IL98000027"
	ERRCODE_IL28  = "IL98000028"
	ERRCODE_IL29  = "IL98000029"
	ERRCODE_IL30  = "IL98000030"
	ERRCODE_IL31  = "IL98000031"
	ERRCODE_IL32  = "IL98000032"
	ERRCODE_IL33  = "IL98000033"
	ERRCODE_IL34  = "IL98000034"
	ERRCODE_IL35  = "IL98000035"
	ERRCODE_IL36  = "IL98000036"
	ERRCODE_IL37  = "IL98000037"
	ERRCODE_IL38  = "IL98000038"
	ERRCODE_IL39  = "IL98000039"
	ERRCODE_IL40  = "IL98000040"
	ERRCODE_IL41  = "IL98000041"
	ERRCODE_IL42  = "IL98000042"
	ERRCODE_IL43  = "IL98000043"
	ERRCODE_IL44  = "IL98000044"
	ERRCODE_IL45  = "IL98000045"
	ERRCODE_IL46  = "IL98000046"
	ERRCODE_IL47  = "IL98000047"
	ERRCODE_IL48  = "IL98000048"
	ERRCODE_IL49  = "IL98000049"
	ERRCODE_IL50  = "IL98000050"
	ERRCODE_IL51  = "IL98000051"
	ERRCODE_IL52  = "IL98000052"
	ERRCODE_IL53  = "IL98000053"
	ERRCODE_IL54  = "IL98000054"
	ERRCODE_IL55  = "IL98000055"
	ERRCODE_IL56  = "IL98000056"
	ERRCODE_IL57  = "IL98000057"
	ERRCODE_IL58  = "IL98000058"
	ERRCODE_IL59  = "IL98000059"
	ERRCODE_IL60  = "IL98000060"
	ERRCODE_IL61  = "IL98000061"
	ERRCODE_IL62  = "IL98000062"
	ERRCODE_IL63  = "IL98000063"
	ERRCODE_IL64  = "IL98000064"
	ERRCODE_IL65  = "IL98000065"
	ERRCODE_IL66  = "IL98000066"
	ERRCODE_IL67  = "IL98000067"
	ERRCODE_IL68  = "IL98000068"
	ERRCODE_IL69  = "IL98000069"
	ERRCODE_IL70  = "IL98000070"
	ERRCODE_IL71  = "IL98000071"
	ERRCODE_IL72  = "IL98000072"
	ERRCODE_IL73  = "IL98000073"
	ERRCODE_IL74  = "IL98000074"
	ERRCODE_IL75  = "IL98000075"
	ERRCODE_IL76  = "IL98000076"
	ERRCODE_IL77  = "IL98000077"
	ERRCODE_IL78  = "IL98000078"
	ERRCODE_IL79  = "IL98000079"
	ERRCODE_IL80  = "IL98000080"
	ERRCODE_IL81  = "IL98000081"
	ERRCODE_IL82  = "IL98000082"
	ERRCODE_IL83  = "IL98000083"
	ERRCODE_IL84  = "IL98000084"
	ERRCODE_IL85  = "IL98000085"
	ERRCODE_IL86  = "IL98000086"
	ERRCODE_IL87  = "IL98000087"
	ERRCODE_IL88  = "IL98000088"
	ERRCODE_IL89  = "IL98000089"
	ERRCODE_IL90  = "IL98000090"
	ERRCODE_IL91  = "IL98000091"
	ERRCODE_IL92  = "IL98000092"
	ERRCODE_IL93  = "IL98000093"
	ERRCODE_IL94  = "IL98000094"
	ERRCODE_IL95  = "IL98000095"
	ERRCODE_IL96  = "IL98000096"
	ERRCODE_IL97  = "IL98000097"
	ERRCODE_IL98  = "IL98000098"
	ERRCODE_IL99  = "IL98000099"
	ERRCODE_IL100 = "IL98000100"
	ERRCODE_IL101 = "IL98000101"
	ERRCODE_IL102 = "IL98000102"
	ERRCODE_IL103 = "IL98000103"
	ERRCODE_IL104 = "IL98000104"
	ERRCODE_IL105 = "IL98000105"
	ERRCODE_IL106 = "IL98000106"
	ERRCODE_IL107 = "IL98000107"
	ERRCODE_IL108 = "IL98000108"
	ERRCODE_IL109 = "IL98000109"
	ERRCODE_IL110 = "IL98000110"
	ERRCODE_IL111 = "IL98000111"
	ERRCODE_IL112 = "IL98000112"
	ERRCODE_IL113 = "IL98000113"
	ERRCODE_IL114 = "IL98000114"
	ERRCODE_IL115 = "IL98000115"
	ERRCODE_IL116 = "IL98000116"
	ERRCODE_IL117 = "IL98000117"
	ERRCODE_IL118 = "IL98000118"
	ERRCODE_IL119 = "IL98000119"
	ERRCODE_IL120 = "IL98000120"
	ERRCODE_IL121 = "IL98000121"
	ERRCODE_IL122 = "IL98000122"
	ERRCODE_IL123 = "IL98000123"
	ERRCODE_IL124 = "IL98000124"
	ERRCODE_IL125 = "IL98000125"
	ERRCODE_IL126 = "IL98000126"
	ERRCODE_IL127 = "IL98000127"
	ERRCODE_IL128 = "IL98000128"
	ERRCODE_IL129 = "IL98000129"
	ERRCODE_IL130 = "IL98000130"
	ERRCODE_IL131 = "IL98000131"
	ERRCODE_IL132 = "IL98000132"
	ERRCODE_IL133 = "IL98000133"
	ERRCODE_IL134 = "IL98000134"
	ERRCODE_IL135 = "IL98000135"
	ERRCODE_IL136 = "IL98000136"
	ERRCODE_IL137 = "IL98000137"
	ERRCODE_IL138 = "IL98000138"
	ERRCODE_IL139 = "IL98000139"
	ERRCODE_IL140 = "IL98000140"
	ERRCODE_IL141 = "IL98000141"
	ERRCODE_IL142 = "IL98000142"
	ERRCODE_IL143 = "IL98000143"
	ERRCODE_IL144 = "IL98000144"
	ERRCODE_IL145 = "IL98000145"
	ERRCODE_IL146 = "IL98000146"
	ERRCODE_IL147 = "IL98000147"
	ERRCODE_IL148 = "IL98000148"
	ERRCODE_IL149 = "IL98000149"
	ERRCODE_IL150 = "IL98000150"
	ERRCODE_IL151 = "IL98000151"
	ERRCODE_IL152 = "IL98000152"
	ERRCODE_IL153 = "IL98000153"
	ERRCODE_IL154 = "IL98000154"
	ERRCODE_IL155 = "IL98000155"
	ERRCODE_IL156 = "IL98000156"
	ERRCODE_IL157 = "IL98000157"
	ERRCODE_IL158 = "IL98000158"
	ERRCODE_IL159 = "IL98000159"
	ERRCODE_IL160 = "IL98000160"
	ERRCODE_IL161 = "IL98000161"
	ERRCODE_IL162 = "IL98000162"
	ERRCODE_IL163 = "IL98000163"
	ERRCODE_IL164 = "IL98000164"
	ERRCODE_IL165 = "IL98000165"
	ERRCODE_IL166 = "IL98000166"
	ERRCODE_IL167 = "IL98000167"
	ERRCODE_IL168 = "IL98000168"
	ERRCODE_IL169 = "IL98000169"
	ERRCODE_IL170 = "IL98000170"
	ERRCODE_IL171 = "IL98000171"
	ERRCODE_IL172 = "IL98000172"
	ERRCODE_IL173 = "IL98000173"
	ERRCODE_IL174 = "IL98000174"
	ERRCODE_IL175 = "IL98000175"
	ERRCODE_IL176 = "IL98000176"
	ERRCODE_IL177 = "IL98000177"
	ERRCODE_IL178 = "IL98000178"
	ERRCODE_IL179 = "IL98000179"
	ERRCODE_IL180 = "IL98000180"
	ERRCODE_IL181 = "IL98000181"
	ERRCODE_IL182 = "IL98000182"
	ERRCODE_IL183 = "IL98000183"
	ERRCODE_IL184 = "IL98000184"
	ERRCODE_IL185 = "IL98000185"
	ERRCODE_IL186 = "IL98000186"
	ERRCODE_IL187 = "IL98000187"
	ERRCODE_IL188 = "IL98000188"
	ERRCODE_IL189 = "IL98000189"
	ERRCODE_IL190 = "IL98000190"
)