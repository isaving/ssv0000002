package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAIL8U01I struct {
	KeprcdNo        string    `json:"KeprcdNo";validate:"required"` //记录编号"`
	SysStusCd       string `json:"SysStusCd,omitempty"`          //系统状态代码  O-日间服务F-日终服务"`
	SysCtofModeCd   string `json:"SysCtofModeCd,omitempty"`      //系统日切模式代码  N-自然日切C-受控日切"`
	OnlineBizDt     string `json:"OnlineBizDt,omitempty"`        //联机业务日期"`
	BatBizDt        string `json:"BatBizDt,omitempty"`           //批量业务日期"`
	NxtoneBizDt     string `json:"NxtoneBizDt,omitempty"`        //下一业务日期"`
	LstoneBizDt     string `json:"LstoneBizDt,omitempty"`        //上一业务日期 "`
	CtofTm          string `json:"CtofTm,omitempty"`             //日切时间  描述事件过程长短和发生顺序的度量,格式:HH:MM:SS,"`
	TranOnlineTm    string `json:"TranOnlineTm,omitempty"`       //转联机时间  描述事件过程长短和发生顺序的度量,格式:HH:MM:SS,"`
	FinlModfyOrgNo  string `json:"FinlModfyOrgNo,omitempty"`     //最后修改机构号"`
	FinlModfyTelrNo string `json:"FinlModfyTelrNo,omitempty"`    //最后修改柜员号  记录最后修改事件的柜员编号,柜员一般包括有交易柜员,复核柜员,授权柜员等不同角色,"`
	TccState        int    `json:"TccState,omitempty"`           //TCC状态
}

type DAIL8U01O struct {
	status string //更新状态
}

type DAIL8U01IDataForm struct {
	FormHead CommonFormHead
	FormData DAIL8U01I
}

type DAIL8U01ODataForm struct {
	FormHead CommonFormHead
	FormData DAIL8U01O
}

type DAIL8U01RequestForm struct {
	Form []DAIL8U01IDataForm
}

type DAIL8U01ResponseForm struct {
	Form []DAIL8U01ODataForm
}

// @Desc Build request message
func (o *DAIL8U01RequestForm) PackRequest(DAIL8U01I DAIL8U01I) (responseBody []byte, err error) {

	requestForm := DAIL8U01RequestForm{
		Form: []DAIL8U01IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAIL8U01I",
				},
				FormData: DAIL8U01I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAIL8U01RequestForm) UnPackRequest(request []byte) (DAIL8U01I, error) {
	DAIL8U01I := DAIL8U01I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAIL8U01I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAIL8U01I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAIL8U01ResponseForm) PackResponse(DAIL8U01O DAIL8U01O) (responseBody []byte, err error) {
	responseForm := DAIL8U01ResponseForm{
		Form: []DAIL8U01ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAIL8U01O",
				},
				FormData: DAIL8U01O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAIL8U01ResponseForm) UnPackResponse(request []byte) (DAIL8U01O, error) {

	DAIL8U01O := DAIL8U01O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAIL8U01O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAIL8U01O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAIL8U01I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
