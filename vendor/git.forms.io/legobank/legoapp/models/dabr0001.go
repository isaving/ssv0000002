package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DABR0001I struct {
	PageRecCount int
	PageNo       int
	BatchType    string
	BatchName    string
	MaxJob       int
	SubmitNum    int
}

type DABR0001O struct {
	PageTotCount int
	PageNo       int
	Records      []DABR0001ORecords
}
type DABR0001ORecords struct {
	BatchName string
	BatchType string
	MaxJob    int
	SubmitNum int
}

type DABR0001IDataForm struct {
	FormHead CommonFormHead
	FormData DABR0001I
}

type DABR0001ODataForm struct {
	FormHead CommonFormHead
	FormData DABR0001O
}

type DABR0001RequestForm struct {
	Form []DABR0001IDataForm
}

type DABR0001ResponseForm struct {
	Form []DABR0001ODataForm
}

// @Desc Build request message
func (o *DABR0001RequestForm) PackRequest(DABR0001I DABR0001I) (responseBody []byte, err error) {

	requestForm := DABR0001RequestForm{
		Form: []DABR0001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DABR0001I",
				},
				FormData: DABR0001I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DABR0001RequestForm) UnPackRequest(request []byte) (DABR0001I, error) {
	DABR0001I := DABR0001I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DABR0001I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DABR0001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DABR0001ResponseForm) PackResponse(DABR0001O DABR0001O) (responseBody []byte, err error) {
	responseForm := DABR0001ResponseForm{
		Form: []DABR0001ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DABR0001O",
				},
				FormData: DABR0001O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DABR0001ResponseForm) UnPackResponse(request []byte) (DABR0001O, error) {

	DABR0001O := DABR0001O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DABR0001O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DABR0001O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DABR0001I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
