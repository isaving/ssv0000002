package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAAUDSH0I struct {
	AcctgNo            string ` validate:"required,max=20"`
	AcctgAcctNo        string ` validate:"required,max=20"`
	ProdType           string
	AcctType           string
	Lvls               float64
	Sigma              float64
	IntRate            string
	IntPlanNo          string
	AcctBal            string
	CumulativeProdAmt  interface{}
	StartDate          string
	AsOfDate           string
	RestCycleStartDate string
	LastMaintDate      string
	LastMaintTime      string
	LastMaintBrno      string
	LastMaintTell      string
}

type DAAUDSH0O struct {
	AcctgNo            string
	AcctgAcctNo        string
	ProdType           string
	AcctType           string
	Lvls               float64
	Sigma              float64
	IntRate            string
	IntPlanNo          string
	AcctBal            string
	CumulativeProdAmt  float64
	StartDate          string
	AsOfDate           string
	RestCycleStartDate string
	LastMaintDate      string
	LastMaintTime      string
	LastMaintBrno      string
	LastMaintTell      string

}

type DAAUDSH0IDataForm struct {
	FormHead CommonFormHead
	FormData DAAUDSH0I
}

type DAAUDSH0ODataForm struct {
	FormHead CommonFormHead
	FormData DAAUDSH0O
}

type DAAUDSH0RequestForm struct {
	Form []DAAUDSH0IDataForm
}

type DAAUDSH0ResponseForm struct {
	Form []DAAUDSH0ODataForm
}

// @Desc Build request message
func (o *DAAUDSH0RequestForm) PackRequest(DAAUDSH0I DAAUDSH0I) (responseBody []byte, err error) {

	requestForm := DAAUDSH0RequestForm{
		Form: []DAAUDSH0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAAUDSH0I",
				},
				FormData: DAAUDSH0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAAUDSH0RequestForm) UnPackRequest(request []byte) (DAAUDSH0I, error) {
	DAAUDSH0I := DAAUDSH0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAAUDSH0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAAUDSH0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAAUDSH0ResponseForm) PackResponse(DAAUDSH0O DAAUDSH0O) (responseBody []byte, err error) {
	responseForm := DAAUDSH0ResponseForm{
		Form: []DAAUDSH0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAAUDSH0O",
				},
				FormData: DAAUDSH0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAAUDSH0ResponseForm) UnPackResponse(request []byte) (DAAUDSH0O, error) {

	DAAUDSH0O := DAAUDSH0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAAUDSH0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAAUDSH0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAAUDSH0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
