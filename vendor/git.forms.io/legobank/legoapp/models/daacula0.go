package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACULA0I struct {
	//输入是个map
}

type DAACULA0O struct {

}

type DAACULA0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACULA0I
}

type DAACULA0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACULA0O
}

type DAACULA0RequestForm struct {
	Form []DAACULA0IDataForm
}

type DAACULA0ResponseForm struct {
	Form []DAACULA0ODataForm
}

// @Desc Build request message
func (o *DAACULA0RequestForm) PackRequest(DAACULA0I DAACULA0I) (responseBody []byte, err error) {

	requestForm := DAACULA0RequestForm{
		Form: []DAACULA0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULA0I",
				},
				FormData: DAACULA0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACULA0RequestForm) UnPackRequest(request []byte) (DAACULA0I, error) {
	DAACULA0I := DAACULA0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACULA0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULA0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACULA0ResponseForm) PackResponse(DAACULA0O DAACULA0O) (responseBody []byte, err error) {
	responseForm := DAACULA0ResponseForm{
		Form: []DAACULA0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULA0O",
				},
				FormData: DAACULA0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACULA0ResponseForm) UnPackResponse(request []byte) (DAACULA0O, error) {

	DAACULA0O := DAACULA0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACULA0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULA0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACULA0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
