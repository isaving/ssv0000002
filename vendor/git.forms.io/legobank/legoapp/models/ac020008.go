package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC020008I struct {
	AcctgAcctNo       string  `validate:"max=32,required"` // 贷款核算账
	PeriodNum         int     `validate:"required"`        //期数
	TradeDate         string  `validate:"max=10"`          // 交易日期
	CurrPeriodRepayDt string  `validate:"max=10,required"` //当期还款日期 curr_period_repay_dt
	PlanRepayBal      float64 `validate:"required"`        //计划还款本金 plan_repay_bal
}

type AC020008O struct {
	AcctgAcctNo              string  `valid:"Required;MaxSize(20)"` //贷款核算账
	PeriodNum                int     `valid:"Required;MaxSize(8)"`  //期数
	AcctgDate                string  `valid:"Required;MaxSize(10)"` //动账日期
	CurrentStatus            string  `valid:"Required;MaxSize(1)"`  //当前记录状态
	CurrPeriodIntacrBgnDt    string  `valid:"Required;MaxSize(10)"` //当期计息开始日期
	CurrPeriodIntacrEndDt    string  `valid:"Required;MaxSize(10)"` //当期计息结束日期
	ValueDate                string  `valid:"Required;MaxSize(10)"` //起息日期
	BurningSum               float64 `valid:"Required"`             //核销金额
	PlanRepayBal             float64 `valid:"Required"`             //计划还款本金
	CurrPeriodRepayDt        string  `valid:"Required;MaxSize(10)"` //当期还款日
	RepayStatus              string  `valid:"Required;MaxSize(2)"`  //本金状态
	CurrentPeriodUnStillPrin float64 `valid:"Required"`             //到期未还本金
	ActualRepayDate          string  `valid:"Required;MaxSize(10)"` //实际还款日期
	ActualRepayBal           float64 `valid:"Required"`             //实际还款本金
}

// @Desc Build request message
func (o *AC020008I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *AC020008I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *AC020008O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC020008O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *AC020008I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

