package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
)

type DAILUDK1IDataForm struct {
	FormHead CommonFormHead
	FormData DAILUDK1I
}

type DAILUDK1ODataForm struct {
	FormHead CommonFormHead
	FormData DAILUDK1O
}

type DAILUDK1RequestForm struct {
	Form []DAILUDK1IDataForm
}

type DAILUDK1ResponseForm struct {
	Form []DAILUDK1ODataForm
}

type DAILUDK1I struct {
	LoanDubilNo                    string  `json:"LoanDubilNo";validate:"required"`
	AcctiAcctNo                    string  `json:"AcctiAcctNo,omitempty"`
	ContrtStusCd                   string  `json:"ContrtStusCd,omitempty"`
	CustNo                         string  `json:"CustNo,omitempty"`
	LoanProdtNo                    string  `json:"LoanProdtNo,omitempty"`
	LoanProdtVersNo                string  `json:"LoanProdtVersNo,omitempty"`
	MakelnOrgNo                    string  `json:"MakelnOrgNo,omitempty"`
	IndvCrtfTypCd                  string  `json:"IndvCrtfTypCd,omitempty"`
	IndvCrtfNo                     string  `json:"IndvCrtfNo,omitempty"`
	RevneCmpdCd                    string  `json:"RevneCmpdCd,omitempty"`
	MakelnManrCd                   string  `json:"MakelnManrCd,omitempty"`
	RelaAcctDtrmnManrCd            string  `json:"RelaAcctDtrmnManrCd,omitempty"`
	RepayDayDtrmnManrCd            string  `json:"RepayDayDtrmnManrCd,omitempty"`
	LoanDeadl                      int     `json:"LoanDeadl,omitempty"`
	LoanDeadlCycCd                 string  `json:"LoanDeadlCycCd,omitempty"`
	PmitAdvRepayTms                int     `json:"PmitAdvRepayTms,omitempty"`
	AdvRepayLimitBgnDt             string  `json:"AdvRepayLimitBgnDt,omitempty"`
	AdvRepayLimitDays              int     `json:"AdvRepayLimitDays,omitempty"`
	LimitTrmInsidPmitPartlRepayFlg string  `json:"LimitTrmInsidPmitPartlRepayFlg,omitempty"`
	LimitTrmInsidPmitPayOffFlg     string  `json:"LimitTrmInsidPmitPayOffFlg,omitempty"`
	OpenAcctDt                     string  `json:"OpenAcctDt,omitempty"`
	FsttmForsprtDt                 string  `json:"FsttmForsprtDt,omitempty"`
	BegintDt                       string  `json:"BegintDt,omitempty"`
	OrgnlMatrDt                    string  `json:"OrgnlMatrDt,omitempty"`
	CurCd                          string  `json:"CurCd,omitempty"`
	LoanAmt                        float64 `json:"LoanAmt,omitempty"`
	EmbFlg                         string  `json:"EmbFlg,omitempty"`
	DecdEmbDt                      string  `json:"DecdEmbDt,omitempty"`
	MansbjTypCd                    string  `json:"MansbjTypCd,omitempty"`
	CtrtNo                         string  `json:"CtrtNo,omitempty"`
	LoanGuarManrCd                 string  `json:"LoanGuarManrCd,omitempty"`
	OthConsmTypCd                  string  `json:"OthConsmTypCd,omitempty"`
	RepayManrCd                    string  `json:"RepayManrCd,omitempty"`
	IntStlDayDtrmnManrCd           string  `json:"IntStlDayDtrmnManrCd,omitempty"`
	AdvRepayTms                    int     `json:"AdvRepayTms,omitempty"`
	TranNormlLoanDt                string  `json:"TranNormlLoanDt,omitempty"`
	BldInstltRepayFlg              string  `json:"BldInstltRepayFlg,omitempty"`
	IntStlPrtyTypCd                string  `json:"IntStlPrtyTypCd,omitempty"`
	LoanIntrtAdjCycCd              string  `json:"LoanIntrtAdjCycCd,omitempty"`
	LoanIntrtAdjCycQty             int     `json:"LoanIntrtAdjCycQty,omitempty"`
	StpDeductFlg                   string  `json:"StpDeductFlg,omitempty"`
	StpDeductRsnTypCd              string  `json:"StpDeductRsnTypCd,omitempty"`
	StpDeductCnfrmDt               string  `json:"StpDeductCnfrmDt,omitempty"`
	LoanIntrtAdjManrCd             string  `json:"LoanIntrtAdjManrCd,omitempty"`
	ExpdayPayOffManrCd             string  `json:"ExpdayPayOffManrCd,omitempty"`
	GraceTrmIntacrFlg              string  `json:"GraceTrmIntacrFlg,omitempty"`
	EvrpridMaxGraceTrmDays         int     `json:"EvrpridMaxGraceTrmDays,omitempty"`
	ContrtPrdGraceTrmTotlDays      int     `json:"ContrtPrdGraceTrmTotlDays,omitempty"`
	AdvRepayColtfeFlg              string  `json:"AdvRepayColtfeFlg,omitempty"`
	ColtfeManrCd                   string  `json:"ColtfeManrCd,omitempty"`
	SglColtfeAmt                   float64 `json:"SglColtfeAmt,omitempty"`
	ColtfeAmtCrdnlnbrCd            string  `json:"ColtfeAmtCrdnlnbrCd,omitempty"`
	ColtfeRatio                    float64 `json:"ColtfeRatio,omitempty"`
	AcrdgRatioColtfeCeilAmt        float64 `json:"AcrdgRatioColtfeCeilAmt,omitempty"`
	AcrdgRatioColtfeFloorAmt       float64 `json:"AcrdgRatioColtfeFloorAmt,omitempty"`
	CurrExecTmprd                  int     `json:"CurrExecTmprd,omitempty"`
	RepayPlanAdjFlg                string  `json:"RepayPlanAdjFlg,omitempty"`
	RestFlg                        string  `json:"RestFlg,omitempty"`
	RestDt                         string  `json:"RestDt,omitempty"`
	TranDvalDt                     string  `json:"TranDvalDt,omitempty"`
	TranDvalFlg                    string  `json:"TranDvalFlg,omitempty"`
	ExtsnTms                       int     `json:"ExtsnTms,omitempty"`
	WaitExtsnFlg                   string  `json:"WaitExtsnFlg,omitempty"`
	CurrLoanRiskClsfCd             string  `json:"CurrLoanRiskClsfCd,omitempty"`
	MatrDt                         string  `json:"MatrDt,omitempty"`
	PayOffDt                       string  `json:"PayOffDt,omitempty"`
	FinlModfyDt                    string  `json:"FinlModfyDt,omitempty"`
	FinlModfyTm                    string  `json:"FinlModfyTm,omitempty"`
	FinlModfyOrgNo                 string  `json:"FinlModfyOrgNo,omitempty"`
	FinlModfyTelrNo                string  `json:"FinlModfyTelrNo,omitempty"`
}

type DAILUDK1O struct {
	Status string `json:"status"`
}

// @Desc Build request message
func (o *DAILUDK1RequestForm) PackRequest(DAILUDK1I DAILUDK1I) (responseBody []byte, err error) {

	requestForm := DAILUDK1RequestForm{
		Form: []DAILUDK1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAILUDK1I",
				},
				FormData: DAILUDK1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAILUDK1RequestForm) UnPackRequest(request []byte) (DAILUDK1I, error) {
	DAILUDK1I := DAILUDK1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAILUDK1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAILUDK1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAILUDK1ResponseForm) PackResponse(DAILUDK1O DAILUDK1O) (responseBody []byte, err error) {
	responseForm := DAILUDK1ResponseForm{
		Form: []DAILUDK1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAILUDK1O",
				},
				FormData: DAILUDK1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAILUDK1ResponseForm) UnPackResponse(request []byte) (DAILUDK1O, error) {

	DAILUDK1O := DAILUDK1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAILUDK1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAILUDK1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}
