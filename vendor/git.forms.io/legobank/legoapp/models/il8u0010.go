package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
)

type IL8U0010IDataForm struct {
	FormHead CommonFormHead
	FormData IL8U0010I
}

type IL8U0010ODataForm struct {
	FormHead CommonFormHead
	FormData IL8U0010O
}

type IL8U0010RequestForm struct {
	Form []IL8U0010IDataForm
}

type IL8U0010ResponseForm struct {
	Form []IL8U0010ODataForm
}

type IL8U0010I struct {
	LoanDubilNo                      string  `json:"LoanDubilNo";validate:"required"`
	CustNo                           string  `json:"CustNo,omitempty"`
	LoanTotlPridnum                  int     `json:"LoanTotlPridnum,omitempty"`
	CurrExecPridnum                  int     `json:"CurrExecPridnum,omitempty"`
	LoanRemainPridnum                int     `json:"LoanRemainPridnum,omitempty"`
	PrinDueBgnDt                     string  `json:"PrinDueBgnDt,omitempty"`   //本金拖欠开始日期
	IntrDueBgnDt                     string  `json:"IntrDueBgnDt,omitempty"`   //利息拖欠开始日期
	AccmDuePridnum                   int     `json:"AccmDuePridnum,omitempty"` //累计拖欠期数
	AccmWrtoffAmt                    float64 `json:"AccmWrtoffAmt,omitempty"`
	LsttrmCompEtimLnRiskClsfDt       string  `json:"LsttrmCompEtimLnRiskClsfDt,omitempty"`
	LsttrmCompEtimLnRiskClsfCd       string  `json:"LsttrmCompEtimLnRiskClsfCd,omitempty"`
	CurtprdCompEtimLnRiskClsfDt      string  `json:"CurtprdCompEtimLnRiskClsfDt,omitempty"`
	CurtprdCompEtimLnRiskClsfCd      string  `json:"CurtprdCompEtimLnRiskClsfCd,omitempty"`
	LsttrmArtgclIdtfyLoanRiskClsfDt  string  `json:"LsttrmArtgclIdtfyLoanRiskClsfDt,omitempty"`
	LsttrmArtgclIdtfyLoanRiskClsfCd  string  `json:"LsttrmArtgclIdtfyLoanRiskClsfCd,omitempty"`
	CurtprdArtgclIdtfyLoanRiskClsfDt string  `json:"CurtprdArtgclIdtfyLoanRiskClsfDt,omitempty"`
	CurtprdArtgclIdtfyLoanRiskClsfCd string  `json:"CurtprdArtgclIdtfyLoanRiskClsfCd,omitempty"`
	FinlModfyDt                      string  `json:"FinlModfyDt,omitempty"`
	FinlModfyTm                      string  `json:"FinlModfyTm,omitempty"`
	FinlModfyOrgNo                   string  `json:"FinlModfyOrgNo,omitempty"`
	FinlModfyTelrNo                  string  `json:"FinlModfyTelrNo,omitempty"`
}

type IL8U0010O struct {
	Status string `json:"status"`
}

// @Desc Build request message
func (o *IL8U0010RequestForm) PackRequest(IL8U0010I IL8U0010I) (responseBody []byte, err error) {

	requestForm := IL8U0010RequestForm{
		Form: []IL8U0010IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL8U0010I",
				},

				FormData: IL8U0010I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL8U0010RequestForm) UnPackRequest(request []byte) (IL8U0010I, error) {
	IL8U0010I := IL8U0010I{}
	if err := json.Unmarshal(request, o); nil != err {
		return IL8U0010I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL8U0010I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL8U0010ResponseForm) PackResponse(IL8U0010O IL8U0010O) (responseBody []byte, err error) {
	responseForm := IL8U0010ResponseForm{
		Form: []IL8U0010ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL8U0010O",
				},
				FormData: IL8U0010O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL8U0010ResponseForm) UnPackResponse(request []byte) (IL8U0010O, error) {

	IL8U0010O := IL8U0010O{}

	if err := json.Unmarshal(request, o); nil != err {
		return IL8U0010O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL8U0010O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

/*func (w *IL8U0010I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
*/
