package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRTB1I struct {
	AcctgAcctNo	string ` validate:"required,max=20"`
}

type DAACRTB1O struct {
	PageTotCount int
	PageNo       int
	Records      []Daacrtb1O

}
type Daacrtb1O struct {
	AcctgAcctNo   string
	PrinStatus    string
	IntPlanNo     string
	OrgCreate     string
	ValidFlag     string
	Bk1           string
	Bk2           string
	Bk3           string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
}


type DAACRTB1IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRTB1I
}

type DAACRTB1ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRTB1O
}

type DAACRTB1RequestForm struct {
	Form []DAACRTB1IDataForm
}

type DAACRTB1ResponseForm struct {
	Form []DAACRTB1ODataForm
}

// @Desc Build request message
func (o *DAACRTB1RequestForm) PackRequest(DAACRTB1I DAACRTB1I) (responseBody []byte, err error) {

	requestForm := DAACRTB1RequestForm{
		Form: []DAACRTB1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTB1I",
				},
				FormData: DAACRTB1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRTB1RequestForm) UnPackRequest(request []byte) (DAACRTB1I, error) {
	DAACRTB1I := DAACRTB1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTB1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTB1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRTB1ResponseForm) PackResponse(DAACRTB1O DAACRTB1O) (responseBody []byte, err error) {
	responseForm := DAACRTB1ResponseForm{
		Form: []DAACRTB1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTB1O",
				},
				FormData: DAACRTB1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRTB1ResponseForm) UnPackResponse(request []byte) (DAACRTB1O, error) {

	DAACRTB1O := DAACRTB1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTB1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTB1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRTB1I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
