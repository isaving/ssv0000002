package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUFZAI struct {
	AcctgAcctNo           string ` validate:"required,max=20"`
	CustId                string
	ContId                string
	ProdCode              string
	OrgId                 string
	BanknoteFlag          string
	AcctType              string
	CustType              string
	DebitFlag             string
	IsAllowOverdraft      string
	Currency              string
	CurrencyMarket        string
	AcctStatus            string
	IsCalInt              string
	AcctOpenDate          string
	FrozenAmt             float64
	ReservedAmt           float64
	LastAcctBal           float64
	CurrentAcctBal        float64
	CurrentAcctBalTemp    float64
	SumCurrentBal         float64
	SumPeriodicBal        float64
	FirstDepDate          string
	LastSubmitDate        string
	CustLastEventDate     string
	LastEventDate         string
	LastRestDate          string
	LastRestBeginDate     string
	LastRestAmt           float64
	NextRestDate          string
	IntPaidAmt            float64
	LastAccruedDate       string
	NextAccrualDate       string
	RestPeriodAccruedDays float64
	AccruedIntAmt         float64
	LastTotDays           string
	DayDebitAmt           float64
	DayDebitCount         float64
	DayCreditAmt          float64
	DayCreditCount        float64
	DayCashInAmt          float64
	DayCashOutAmt         float64
	DayTranInAmt          float64
	DayTranOutAmt         float64
	DayTranCnt            float64
	LastMaintDate         string
	LastMaintTime         string
	LastMaintBrno         string
	LastMaintTell         string
	DeductFlag            string
	IntrPlanNo            string

	HostTranSerialNo          string
	BussDate                  string
	BussTime                  string
	HostTranSeq               int64
	PeripheralSysWorkday      string
	PeripheralSysWorktime     string
	PeripheralTranSerialNo    string
	PeripheralTranSeq         float64
	BookedWorkday             string
	AcctingTime               string
	LiquidationDate           string
	LiquidationTime           string
	TranOrgId                 string
	AgentOrgId                string
	TranTeller                string
	AuthTeller                string
	ReviewTeller              string
	TranChannel               string
	AccessChannel             string
	TerminalNo                string
	BussSys                   string
	TrasactionCode            string
	FunctionCode              string
	ApprovalNo                string
	ClearingBussType          string
	ProdSeq                   float64
	BussType                  string
	BussCategories            string
	LocalBankFlag             string
	Country                   string
	OtherBankFlag             string
	AcctingOrgId              string
	LocalAcctDiff             string
	LocalTranDetailType       string
	TheMerchantNo             string
	BussAcctNo                string
	AcctNo                    string
	SubjectNo                 string
	SubjectBreakdown          string
	MediaType                 string
	LocalMediaPrefix          string
	MediaNo                   string
	CustDiff                  string
	AmtType                   string
	DurationOfDep             float64
	DaysOfDep                 float64
	AcctingCode1              string
	AcctingCode2              string
	AcctingCode3              string
	AcctingCode4              string
	EventIndication           string
	EventBreakdown            string
	CustEvent1                string
	CustEvent2                string
	Amt                       float64
	AcctBal                   float64
	CashTranFlag              string
	ValueDate                 string
	ExchRestType              string
	AntiTradingMark           string
	CorrectTradingFlag        string
	OriginalTradingDay        string
	OriginalHostTranSerialNo  float64
	OriginalTradeTellerNo     string
	AcctManagerNo             string
	MessageCode               string
	AbstractCode              string
	UseCode                   string
	SetNo                     string
	WhetherToAllowPunching    string
	ChannelCorrectControlFlag string
	IsSendClearingFlag        string
	IsMakeUpForPayables       string
}

type DAACUFZAO struct {

}

type DAACUFZAIDataForm struct {
	FormHead CommonFormHead
	FormData DAACUFZAI
}

type DAACUFZAODataForm struct {
	FormHead CommonFormHead
	FormData DAACUFZAO
}

type DAACUFZARequestForm struct {
	Form []DAACUFZAIDataForm
}

type DAACUFZAResponseForm struct {
	Form []DAACUFZAODataForm
}

// @Desc Build request message
func (o *DAACUFZARequestForm) PackRequest(DAACUFZAI DAACUFZAI) (responseBody []byte, err error) {

	requestForm := DAACUFZARequestForm{
		Form: []DAACUFZAIDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUFZAI",
				},
				FormData: DAACUFZAI,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUFZARequestForm) UnPackRequest(request []byte) (DAACUFZAI, error) {
	DAACUFZAI := DAACUFZAI{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUFZAI, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUFZAI, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUFZAResponseForm) PackResponse(DAACUFZAO DAACUFZAO) (responseBody []byte, err error) {
	responseForm := DAACUFZAResponseForm{
		Form: []DAACUFZAODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUFZAO",
				},
				FormData: DAACUFZAO,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUFZAResponseForm) UnPackResponse(request []byte) (DAACUFZAO, error) {

	DAACUFZAO := DAACUFZAO{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUFZAO, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUFZAO, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUFZAI) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
