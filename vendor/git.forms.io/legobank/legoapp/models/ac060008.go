package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC060008I struct {
	Op                  string `valid:"MaxSize(1)"`
	IntacrRuleType      string `valid:"MaxSize(2)"`   //计息规则类型
	IntacrRuleKind      string `valid:"MaxSize(3)"`   //计息规则种类
	AcctgAcctNo         string `valid:"MaxSize(20)"`  //贷款核算账号
	PrinStatus          string `valid:"MaxSize(1)"`   //本金状态
	IntPlanNo           string `valid:"MaxSize(20)"`  //计息规则编号(新增是没有这个字段)
	BankNo              string `valid:"MaxSize(20)"`  //机构号
	Currency            string `valid:"MaxSize(3)"`   // 币种代码
	IntFlag             string `valid:"MaxSize(1)"`   //计息优先类型代码
	IntCalcOption       string `valid:"MaxSize(2)"`   //利息计算选项
	MinIntAmt           float64                       //`valid:"Required"`              //最低结息金额 19
	LyrdFlag            string `valid:"MaxSize(1)"`   //分层标志   只有存款使用
	AmtLvlsFlag         string `valid:"MaxSize(1)"`   //金额层级类型代码  只有存款使用
	TermLvlsFlag        string `valid:"MaxSize(1)"`   //期限层级类型代码  只有存款使用
	TermAmtPrtyFlag     string `valid:"MaxSize(1)"`   //期限金额优先类型代码  只有存款使用
	IntRateUseFlag      string `valid:"MaxSize(1)"`   //利率使用标识
	IntRateNo           string `valid:"MaxSize(20)"`  //利率编号
	FixdIntRate         float64                       //`valid:"Required"`              //固定利率 9 6
	MinIntRate          float64                       //`valid:"Required"`              //最低利率 9 6
	MonIntUnit          string `valid:"MaxSize(1)"`   //月计息天数代码
	YrIntUnit           string `valid:"MaxSize(1)"`   //年计息天数代码
	FloatOption         string `valid:"MaxSize(1)"`   //浮动种类代码
	FloatFlag           string `valid:"MaxSize(2)"`   //浮动方向代码
	FloatCeil           float64                       //`valid:"Required"`              //浮动上限值 19
	FloatFloor          int                           //`valid:"Required"`              //浮动下限值
	DefFloatRate        float64                       //`valid:"Required"`              //默认浮动值 10
	CornerFlag          string `valid:"MaxSize(1)"`   //分角位参与计息标志
	CritocalPointFlag   string `valid:"MaxSize(1)"`   //临界点区间分类代码
	LvlsIntRateAmtType  string `valid:"MaxSize(2)"`   //层级利率金额类型代码
	TolLvls             int                           //`valid:"Required"`              //层级总数量
	LyrdTermUnit        string `valid:"MaxSize(2)"`   //分层期限单位代码
	LoanIntRateAdjType  string `valid:"MaxSize(2)"`   //使用利率类型代码
	LoanIntRateAdjCycle string `valid:"MaxSize(1)"`   //贷款利率调整周期代码
	LoanIntRateAdjFreq  string `valid:"MaxSize(1)"`   //贷款利率调整周期数量
	IntPlanDesc         string `valid:"MaxSize(200)"` //计息规则描述
}

type AC060008O struct {
	AcctgAcctNo string
	IntPlanNo string
}

type AC060008IDataForm struct {
	FormHead CommonFormHead
	FormData AC060008I
}

type AC060008ODataForm struct {
	FormHead CommonFormHead
	FormData AC060008O
}

type AC060008RequestForm struct {
	Form []AC060008IDataForm
}

type AC060008ResponseForm struct {
	Form []AC060008ODataForm
}

// @Desc Build request message
func (o *AC060008RequestForm) PackRequest(AC060008I AC060008I) (responseBody []byte, err error) {

	requestForm := AC060008RequestForm{
		Form: []AC060008IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060008I",
				},
				FormData: AC060008I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC060008RequestForm) UnPackRequest(request []byte) (AC060008I, error) {
	AC060008I := AC060008I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC060008I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060008I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC060008ResponseForm) PackResponse(AC060008O AC060008O) (responseBody []byte, err error) {
	responseForm := AC060008ResponseForm{
		Form: []AC060008ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060008O",
				},
				FormData: AC060008O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC060008ResponseForm) UnPackResponse(request []byte) (AC060008O, error) {

	AC060008O := AC060008O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC060008O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060008O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC060008I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
