package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRTC2I struct {
	AcctgAcctNo		string `validate:"required,max=40"`
	BalanceType		string
}

type DAACRTC2O struct {
	Records []DAACRTC3ORecords
	/*RecordNo            int64
	AcctgAcctNo         string
	BalanceType         string
	PeriodNum           int64
	CurrIntStDate       string
	CurrIntEndDate      string
	UnpaidInt           float64
	RepaidInt           float64
	AccmWdAmt           float64
	AccmIntSetlAmt      float64
	UnpaidIntAmt        float64
	RepaidIntAmt        float64
	AlrdyTranOffshetInt float64
	DcValueInt          float64
	CavInt              float64
	OnshetInt           float64
	FrzAmt              float64
	Status              string
	LastCalcDate        string
	LastMaintDate       string
	LastMaintTime       string
	LastMaintBrno       string
	LastMaintTell       string
	AcruUnstlIntr       float64
	AccmCmpdAmt         float64*/

}

type DAACRTC2IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRTC2I
}

type DAACRTC2ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRTC2O
}

type DAACRTC2RequestForm struct {
	Form []DAACRTC2IDataForm
}

type DAACRTC2ResponseForm struct {
	Form []DAACRTC2ODataForm
}

// @Desc Build request message
func (o *DAACRTC2RequestForm) PackRequest(DAACRTC2I DAACRTC2I) (responseBody []byte, err error) {

	requestForm := DAACRTC2RequestForm{
		Form: []DAACRTC2IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTC2I",
				},
				FormData: DAACRTC2I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRTC2RequestForm) UnPackRequest(request []byte) (DAACRTC2I, error) {
	DAACRTC2I := DAACRTC2I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTC2I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTC2I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRTC2ResponseForm) PackResponse(DAACRTC2O DAACRTC2O) (responseBody []byte, err error) {
	responseForm := DAACRTC2ResponseForm{
		Form: []DAACRTC2ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTC2O",
				},
				FormData: DAACRTC2O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRTC2ResponseForm) UnPackResponse(request []byte) (DAACRTC2O, error) {

	DAACRTC2O := DAACRTC2O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTC2O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTC2O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRTC2I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
