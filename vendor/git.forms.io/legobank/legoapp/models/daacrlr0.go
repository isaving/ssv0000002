package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRLR0I struct {
	BizFolnNo	string
	SysFolnNo	string
}

type DAACRLR0O struct {
	AcctgAcctNo   string      `json:"AcctgAcctNo"`
	AcctingOrgID  interface{} `json:"AcctingOrgId"`
	BalanceType   interface{} `json:"BalanceType"`
	BizFolnNo     string      `json:"BizFolnNo"`
	ChnlType      interface{} `json:"ChnlType"`
	Currency      string      `json:"Currency"`
	IntRepayAmt   interface{} `json:"IntRepayAmt"`
	IntRepayType  interface{} `json:"IntRepayType"`
	LastMaintBrno interface{} `json:"LastMaintBrno"`
	LastMaintDate interface{} `json:"LastMaintDate"`
	LastMaintTell interface{} `json:"LastMaintTell"`
	LastMaintTime interface{} `json:"LastMaintTime"`
	MgmtOrgID     interface{} `json:"MgmtOrgId"`
	PeriodNum     interface{} `json:"PeriodNum"`
	RecordNo      int         `json:"RecordNo"`
	Status        string      `json:"Status"`
	SysFolnNo     string      `json:"SysFolnNo"`
	TccState      int         `json:"TccState"`
	WorkDate      interface{} `json:"WorkDate"`
}

type DAACRLR0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRLR0I
}

type DAACRLR0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRLR0O
}

type DAACRLR0RequestForm struct {
	Form []DAACRLR0IDataForm
}

type DAACRLR0ResponseForm struct {
	Form []DAACRLR0ODataForm
}

// @Desc Build request message
func (o *DAACRLR0RequestForm) PackRequest(DAACRLR0I DAACRLR0I) (responseBody []byte, err error) {

	requestForm := DAACRLR0RequestForm{
		Form: []DAACRLR0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLR0I",
				},
				FormData: DAACRLR0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRLR0RequestForm) UnPackRequest(request []byte) (DAACRLR0I, error) {
	DAACRLR0I := DAACRLR0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLR0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLR0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRLR0ResponseForm) PackResponse(DAACRLR0O DAACRLR0O) (responseBody []byte, err error) {
	responseForm := DAACRLR0ResponseForm{
		Form: []DAACRLR0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLR0O",
				},
				FormData: DAACRLR0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRLR0ResponseForm) UnPackResponse(request []byte) (DAACRLR0O, error) {

	DAACRLR0O := DAACRLR0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLR0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLR0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRLR0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
