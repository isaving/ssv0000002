package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type CM070001RequestForm struct {
	Form []CM070001IDataForm
}

type CM070001IDataForm struct {
	FormHead CommonFormHead
	FormData CM070001I
}
type CM070001I struct {
	/*CrdtAplySn string `Required;valid:"MaxSize(34)"` //授信申请流水号  // Credit application serial number
	CustNo               string `valid:"MaxSize(14)"` // 客户编号*/
	UserId int64
	Api string
}

// @Desc Parsing request message
func (w *CM070001RequestForm) UnPackRequest(req []byte) (CM070001I, error) {
	CM070001I := CM070001I{}
	if err := json.Unmarshal(req, w); nil != err {
		return CM070001I, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	if len(w.Form) < 1 {
		return CM070001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return w.Form[0].FormData, nil
}

func (w *CM070001RequestForm) PackRequest(CM070001IData CM070001I) (res []byte, err error) {
	resForm := CM070001RequestForm{
		Form: []CM070001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "CM070001I001",
				},
				FormData: CM070001IData,
			},
		},
	}

	rspBody, err := json.Marshal(resForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, "")
	}

	return rspBody, nil
}





type CM070001ResponseForm struct {
	Form []CM070001ODataForm
}

type CM070001ODataForm struct {
	FormHead CommonFormHead
	FormData CM070001O
}

type CM070001O struct {
	/*TotalPageNum int
	PageNo       int
	CustomerInfo []Records*/
	/*AvailQuotaAmt  int         `json:"AvailQuotaAmt"`
	ChgAplyNo      interface{} `json:"ChgAplyNo"`
	CurCd          string      `json:"CurCd"`
	CurrCrdtQta    int         `json:"CurrCrdtQta"`
	CustNm         string      `json:"CustNm"`
	CustNo         string      `json:"CustNo"`
	ProdId         string      `json:"ProdId"`
	ProdName       string      `json:"ProdName"`
	QtaAprvlDt     string      `json:"QtaAprvlDt"`
	QtaMatrDt      string      `json:"QtaMatrDt"`
	QtaStusCd      string      `json:"QtaStusCd"`
	RevlQtaFlg     string      `json:"RevlQtaFlg"`
	WhtlPreCrdtQta int         `json:"WhtlPreCrdtQta"`*/
	Status string   `json:"Status"`
}



// @Desc Build response message
func (w *CM070001ResponseForm) PackResponse(Data CM070001O) (res []byte, err error) {
	resForm := CM070001ResponseForm{
		Form: []CM070001ODataForm{
			CM070001ODataForm{
				FormHead: CommonFormHead{
					FormId: "CM070001O",
				},
				FormData: Data,
			},
		},
	}

	rspBody, err := json.Marshal(resForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

func (w *CM070001I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
