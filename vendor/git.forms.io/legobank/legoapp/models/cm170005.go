package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type CM170005I struct {
	BatchType string
}

type CM170005O struct {
	PageTotCount int
	PageNo       int
	Records      []DABR0001ORecords
}
type CM170005IDataForm struct {
	FormHead CommonFormHead
	FormData CM170005I
}

type CM170005ODataForm struct {
	FormHead CommonFormHead
	FormData CM170005O
}

type CM170005RequestForm struct {
	Form []CM170005IDataForm
}

type CM170005ResponseForm struct {
	Form []CM170005ODataForm
}

// @Desc Build request message
func (o *CM170005RequestForm) PackRequest(CM170005I CM170005I) (responseBody []byte, err error) {

	requestForm := CM170005RequestForm{
		Form: []CM170005IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "CM170005I",
				},
				FormData: CM170005I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *CM170005RequestForm) UnPackRequest(request []byte) (CM170005I, error) {
	CM170005I := CM170005I{}
	if err := json.Unmarshal(request, o); nil != err {
		return CM170005I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return CM170005I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *CM170005ResponseForm) PackResponse(CM170005O CM170005O) (responseBody []byte, err error) {
	responseForm := CM170005ResponseForm{
		Form: []CM170005ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "CM170005O",
				},
				FormData: CM170005O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *CM170005ResponseForm) UnPackResponse(request []byte) (CM170005O, error) {

	CM170005O := CM170005O{}

	if err := json.Unmarshal(request, o); nil != err {
		return CM170005O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return CM170005O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *CM170005I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
