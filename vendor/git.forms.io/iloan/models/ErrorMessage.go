package models

type ErrorMessageForm struct {
	Form []ErrorMessageData
}

type ErrorMessageData struct {
	FormHead FormHead
	FormData map[string]string
}
type ErrorMessageRespForm struct {
	Form []struct{
		FormHead FormHead
		FormData interface{}
	}
}

type  ErrorMessage struct{
	InnerCode string
	MessageEN string
	MessageCN string
	MessageTH string
}
type ErrorMessageRespForm2 struct {
	Form []struct{
		FormHead FormHead
		FormData struct {
			Records       map[string]ErrorMessage
			PageTotCount string
			PageNo       string
		}
	}
}