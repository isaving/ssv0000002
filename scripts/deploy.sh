#!/bin/bash

[ -d deploy ] || mkdir deploy
  rm -rf deploy/*
[ $# -ne 2 ] && {
	path=$(
		cd "$(dirname "$0")"
		pwd
	)
	project=$(echo ${path##*/}|awk '{print tolower($0)}')
	group=$(echo $path | rev | cut -d '/' -f 2 | rev)
} || {
	group=$1
	project=$(echo $2|awk '{print tolower($0)}')
}

[ -d deploy/$group ] || mkdir deploy/$group
cat <<EOF >deploy/$group/package.json
{
	"name":"$group",
	"description":"$group public chart template",
	"version":"0.0.1",
	"namespace": "default",
	"type":"legosvr",
	"permission": "public",
	"resource": {
		"cpu": 2,
		"memory": "16Gi",
		"disk": "20Gi"
	},
	"applications":[
		{
			"version":"0.0.1",
			"apolloAppId":"",
			"index":1,
			"necessary":true,
			"name": "$project"
		}
	],
	"images":[],
	"ports":[],
	"dependencies":[]
}
EOF

echo "create deploy/$group/package.json success"

mkdir deploy/$group/$project

cat <<EOF >deploy/$group/$project/values.yaml
## The image repository
image: "$group/$project"

## The  image tag
imageTag: "v0.0.1"  # Confluent image for Kafka 2.0.0

commImage: "hub.universe.com/universe/comm-agent"
commImageTag: "press-v1.48"

## Specify a imagePullPolicy
imagePullPolicy: "IfNotPresent"

## The StatefulSet installs 3 pods by default
minInstNum: 1

## Load dls configuration, default num 0 don't load ,num 1 load, old config num 9.
dlsFlag: 1
EOF

echo "create deploy/$group/$project/values.yaml success"
confirm=$(cat conf/app.conf|grep topic.confirmName| awk -F= '{print $2}'|sed 's/ //g')
cancel=$(cat conf/app.conf|grep topic.cancelName| awk -F= '{print $2}'|sed 's/ //g')
cat <<EOF >deploy/$group/$project/data.json
[
	{
		"section": "Config",
		"upgradable": true,
		"keys": [
			{
				"label": "Commagent Image Tag",
				"necessary": true,
				"notice": "comm agent image tag",
				"key": "commImageTag",
				"type": "input",
				"default": "press-v1.48",
				"dataType": "text"
			},
			{
				"label": "Dls Flag",
				"upgradable": true,
				"necessary": true,
				"notice": "num=0/1, 1 load dls configuration,0 do not load",
				"key": "dlsFlag",
				"regular": "",
				"type": "input",
				"default": "1",
				"dataType": "number"
			}
		]
	}
]
EOF
echo "create deploy/$group/$project/data.json success"
cat <<EOF >deploy/$group/$project/Chart.yaml
name: $project
version: 0.0.1
appVersion: 0.0.1
description: A Helm chart for Kubernetes
keywords:
- engine
home: http://intellif.io/
icon: http://pmo4b1dce.pic37.websiteonline.cn/upload/if_web_logo_wu2d.png
maintainers:
- name: intellif
  email: ci@intellif.com
engine: gotpl
EOF
echo "create deploy/$group/$project/Chart.yaml success"
[ -d deploy/$group/$project/templates ] || mkdir deploy/$group/$project/templates
[ -f deploymountfile.tmp ] && rm deploymountfile.tmp
[ -f deploymountvolumkey.tmp ] && rm deploymountvolumkey.tmp
for i in $(ls conf | grep -v app.conf); do
	cat <<EOF >>deploymountfile.tmp
            - name: config-app
              mountPath: /data/app/conf/$i
              subPath: $i
EOF
	cat <<EOF >>deploymountvolumkey.tmp
              - key: $i
                path: $i
EOF
done

cat <<EOF >deploy/$group/$project/templates/deploy.yaml
apiVersion: apps/v1beta2
kind: StatefulSet
metadata:
  name: {{ .Values.serviceName }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Values.serviceName }}
    chart: "{{ .Chart.Name }}-{{ .Chart.Version }}"
    release: "{{ .Release.Name }}"
    heritage: "{{ .Release.Service }}"
spec:
  serviceName: {{ .Values.serviceName }}
  selector:
    matchLabels:
      app: {{ .Values.serviceName }}
  replicas: {{ .Values.minInstNum}}
  template:
    metadata:
      labels:
        app: {{ .Values.serviceName }}
    spec:
      containers:
        - name: comm-agent
          image: "{{ .Values.commImage }}:{{ .Values.commImageTag }}"
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          volumeMounts:
            - name: config-comm
              mountPath: /data/comm/conf/app.conf
              subPath: app.conf
            - name: config-comm
              mountPath: /data/comm/conf/mq_session.json
              subPath: mq_session.json
            - name: host-time
              mountPath: /etc/localtime
              readOnly: true
            - mountPath: /data/commlogs
              name: commlog-path
          resources:
            requests:
              cpu: 0
              memory: 0
            limits:
              cpu: 1400m
              memory: 1Gi
          env:
            - name: NODE_ID
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            - name: INSTANCE_ID
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
        - name: {{ .Values.serviceName }}
          image: "hub.universe.com/{{ .Values.image }}:{{ .Values.imageTag }}"
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          volumeMounts:
            - name: config-app
              mountPath: /data/app/conf/app.conf
              subPath: app.conf
$([ -s deploymountfile.tmp ] && cat deploymountfile.tmp)
            - name: host-time
              mountPath: /etc/localtime
              readOnly: true
            - name: log-path
              mountPath: /data/logs
          resources:
            requests:
              cpu: 0
              memory: 0
            limits:
              cpu: 1200m
              memory: 4Gi
          env:
            - name: NODE_ID
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            - name: INSTANCE_ID
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            {{- range \$key, \$value := .Values.env }}
            - name: "{{ \$key }}"
              value: "{{ \$value }}"
            {{- end }}
      volumes:
        - name: config-comm
          configMap:
            name: "{{ .Values.serviceName }}-agentconfig"
            items:
              - key: app.conf
                path: app.conf
              - key: mq_session.json
                path: mq_session.json
        - name: config-app
          configMap:
            name: "{{ .Values.serviceName }}-config"
            items:
              - key: app.conf
                path: app.conf
$([ -s deploymountvolumkey.tmp ] && cat deploymountvolumkey.tmp)
        - name: host-time
          hostPath:
            path: /etc/localtime
        - name: log-path
          hostPath:
            path: "/data/logs/{{ .Values.serviceName }}"
        - name: commlog-path
          hostPath:
            path: "/data/commlogs/{{ .Values.serviceName }}"
EOF
echo "create deploy/$group/$project/templates/deploy.yaml success"
cat <<EOF >deploy/$group/$project/templates/comm.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: "{{ .Values.serviceName }}-agentconfig"
  namespace: {{ .Values.namespace }}
data:
  app.conf: |
    appname = comm-agent
    httpaddr = "127.0.0.1"
    httpport = 18080
    runmode = dev
    autorender = false
    copyrequestbody = true
    replyChannelNum = 1024
    dlsLookup = true
    commondcn = {{ .Values.dcn }}
    openAPMFlag = true
    dasprefix = DA

    [log]
    logLevel = error
    logFile  =./comm-agent.log
    logLevelUnixSocket = /var/comm-agent

    [client]
    callbackUrl=http://127.0.0.1:18082/v1/newmsg

    [mq_session]
    configFilePath = ./conf/mq_session.json

    [app]
    session_name = default
    org = {{ .Values.org }}
    az = {{ .Values.az }}
    dcn = {{ .Values.dcnInstance }}
    serviceId = {{ .Values.serviceId }}
    name = comm-agent
    dlsflag = {{ .Values.dlsFlag }}

    [dls_query]
    agent_config=DlscmAgentConf

    [crypto]
    enable = false
    algo = AES256
    mode = GCM
    padding = "PKCS7"
    upstreamServices =
    exclusionsTopics = rapm000;KmsGetKeys;DTS_AGENT_REGISTER;DTS_AGENT_ENLIST;DTS_AGENT_TRY_RESULT_REPORT;dls_usecase
    getServicesKeysTopic = KmsGetKeys

  mq_session.json: |
    [
      {
        "driver": "solace",
        "name": "default",
        "attributes": {
            "SESSION_VPN_NAME": "{{ .Values.vpnName }}",
            "SESSION_USERNAME": "{{ .Values.vpnUsername }}",
            "SESSION_PASSWORD": "{{ .Values.vpnPassword }}",
            "SESSION_HOST": "tcp:{{ .Values.vpnVip }}"
        },
        "topics": {{ .Values.topicInfo }},
        "queues": {{ .Values.queueInfo }}
      }
    ]
EOF
echo "create deploy/$group/$project/templates/comm.yaml success"
[ -f $group.$project.tmp ] && rm $group.$project.tmp

for i in $(ls conf); do
	echo "  "$i: \|+ >>configmap.tmp
	cat conf/$i | sed "s/appname.*/appname = {{ .Values.serviceName }}/g" | sed "s/callback_port.*/callback_port = 18082/g" | sed "s/comm_agent_address.*/comm_agent_address = http:\/\/127.0.0.1:18080/g" | sed "s/logFileRootPath.*/logFileRootPath=.\/logs/g" | sed "s/logFile\ =.*/logFile = {{ .Values.serviceName }}.log/g" | sed "s/logLevelUnixSocket.*/logLevelUnixSocket=\/tmp\/{{ .Values.serviceName }}.sock/g" | sed "s/organization.*/organization = {{ .Values.org }}/g" | sed "s/groupDcn.*/groupDcn = {{ .Values.dcn }}/g" | sed "s/dataCenterNode.*/dataCenterNode = {{ .Values.dcnInstance }}/g" | sed "s/topic.confirmName.*/topic.confirmName = {{ .Values.dtsConfirmTopic }}/g" | sed "s/topic.cancelName.*/topic.cancelName = {{ .Values.dtsCancelTopic }}/g" | sed "s/^/    /g" >>configmap.tmp
	echo "" >>configmap.tmp
	echo "" >>configmap.tmp
done

cat <<EOF >deploy/$group/$project/templates/app.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: "{{ .Values.serviceName }}-config"
  namespace: {{ .Values.namespace }}
data:
$(cat configmap.tmp)

EOF
echo "create deploy/$group/$project/templates/app.yaml success"

rm configmap.tmp
rm deploymountfile.tmp
rm deploymountvolumkey.tmp
