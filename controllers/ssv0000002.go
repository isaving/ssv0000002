//Version: v0.01
package controllers

import (
	"git.forms.io/isaving/sv/ssv0000002/models"
	"git.forms.io/isaving/sv/ssv0000002/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv0000002Controller struct {
	controllers.CommController
}

func (*Ssv0000002Controller) ControllerName() string {
	return "Ssv0000002Controller"
}

// @Desc ssv0000002 controller
// @Description Entry
// @Param ssv0000002 body models.SSV0000002I true "body for user content"
// @Success 200 {object} models.SSV0000002O
// @router /ssv0000002 [post]
// @Author
// @Date 2020-12-04
func (c *Ssv0000002Controller) Ssv0000002() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv0000002Controller.Ssv0000002 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv0000002I := &models.SSV0000002I{}
	if err := models.UnPackRequest(c.Req.Body, ssv0000002I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv0000002I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv0000002 := &services.Ssv0000002Impl{}
	ssv0000002.New(c.CommController)
	ssv0000002.Sv000002I = ssv0000002I

	ssv0000002O, err := ssv0000002.Ssv0000002(ssv0000002I)

	if err != nil {
		log.Errorf("Ssv0000002Controller.Ssv0000002 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv0000002O.PackResponse()
	if err != nil {
		c.SetServiceError(err)
		return
	}
	c.SetAppBody(responseBody)
}

// @Title Ssv0000002 Controller
// @Description ssv0000002 controller
// @Param Ssv0000002 body models.SSV0000002I true body for SSV0000002 content
// @Success 200 {object} models.SSV0000002O
// @router /create [post]
/*func (c *Ssv0000002Controller) SWSsv0000002() {
	//Here is to generate API documentation, no need to implement methods
}*/
