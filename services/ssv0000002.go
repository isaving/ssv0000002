//Version: v0.01
package services

import (
	"git.forms.io/isaving/sv/ssv0000002/constant"
	"git.forms.io/isaving/sv/ssv0000002/models"
	"git.forms.io/legobank/legoapp/services"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/go-errors/errors"
)

type Ssv0000002Impl struct {
	services.CommonService
	//TODO ADD Service Self Define Field
	Sv900003O *models.SSV9000003O //SV900003O
	Sv900003I *models.SSV9000003I //SV900003I
	Sv000002O *models.SSV0000002O //SV000002O
	Sv000002I *models.SSV0000002I //SV000002I
}

// @Desc Ssv0000002 process
// @Author
// @Date 2020-12-04
func (impl *Ssv0000002Impl) Ssv0000002(ssv0000002I *models.SSV0000002I) (ssv0000002O *models.SSV0000002O, err error) {

	impl.Sv000002I = ssv0000002I
	impl.Sv000002O = &models.SSV0000002O{}
	//TODO Service Business Process

	//调用存款产品销售信息查询DAS-产品基础信息表 ssv9000003
	err = impl.CallSv9000003()
	if err != nil {
		log.Debug("调用sv900003错误！")
		return nil, err
	}
	ssv0000002O = &models.SSV0000002O{
		//TODO Assign  value to the OUTPUT Struct field
		RecordTotNum: impl.Sv000002O.RecordTotNum,
		Records:      impl.Sv000002O.Records,
	}
	return ssv0000002O, nil
}

func (impl *Ssv0000002Impl) CallSv9000003() error {
	impl.Sv000002O = &models.SSV0000002O{}
	SSV9000003I := models.SSV9000003I{
		PrductStatus: impl.Sv000002I.PrductStatus,
	}
	reqBody, err := models.PackRequest(SSV9000003I)
	if nil != err {
		return errors.Errorf("%v", err)
	}
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_SCM, constant.DLS_ID_COMMON, constant.SV000003, reqBody)
	if err != nil {
		log.Error("调用SV000002")
		return err
	}

	QuerySSV9000002O := models.SSV0000002O{}
	err = QuerySSV9000002O.UnPackResponse(resBody)
	if err != nil {
		log.Info("解包错误！")
		return err
	}
	log.Debug("QuerySSV9000002O.Records:", QuerySSV9000002O.Records)
	impl.Sv000002O.Records = QuerySSV9000002O.Records
	impl.Sv000002O.RecordTotNum = QuerySSV9000002O.RecordTotNum
	return nil
}
